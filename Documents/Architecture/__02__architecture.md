# Testers Project

Testers Porject Folder & file Architecture 

```plantuml
@startuml
folder root [
    プロジェクトルート
]
folder Client [
    クライアント開発
    ---
    
    ===
    含まれているライブラリー
    Angular
    Angular-Material
    Electron
]
folder Server [
    サーバサイド開発
    ---
    
    ===
    含まれているライブラリー
    Rails 5.2
]
folder Documents [
    設計書・試験書 etc
]
folder Architecture [
    __01__requirement.md
    __02__architecture.md
    __03__common_rule.md
]
folder Doc_Client [

]
folder Doc_Server [

]
database database[
    SQLite3
    ===

    テスティングデータ
]
root *--- Client
root *--- Server
root *--- Documents
Documents *--- Architecture
Documents *--- Doc_Client
Documents *--- Doc_Server
Server *--- database
@enduml
```