# Testers UseCase

Testers ProjectのUseCase
testers　
```plantuml
@startuml
left to right direction
actor テスト担当者 as tester
actor Testers_System as testers
rectangle Testers {
tester --> (サービス登録)
tester --> (画面登録)
tester --> (画面オブジェクト登録)
tester --> (シナリオ登録)
tester --> (テスト登録)
tester -> (テスト実行)
(サービス登録) <--- testers
(画面登録) <--- testers
(画面オブジェクト登録) <--- testers
(シナリオ登録) <--- testers
(テスト登録) <--- testers
(テストコード生成) <-- testers
(テストアーカイブ) <-- testers
@enduml
```