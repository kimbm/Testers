# What Testers Project ?

Testersプロジェクトは常に存在する自動テストツールをもっとより簡単に、もっと汎用的に使えるようにすることを目的としている


## プロジェクトの目的

Testersプロジェクトの目的は「Enterprise Application」をテストする際、テスト担当者はTestersシステムを利用して、
スクリプトを手軽に作成する。また、Enterprise Applicationの仕様変更に応じてテストスクリプトのメンテナンスを
手軽に行いテストにかかるコストを下げることを目標とする



## Work flow

Testers プロジェクトの業務の流れは主にテストする対象を登録し、登録したEnterprise Applicationをテストすることである。テスト担当者は以下の業務を行うことができる。

```plantuml
@startuml
scale 6/7
| テスト担当者　|
start
:テスト登録;
|Client Application|
:サービス登録画面表示;
| テスト担当者　|
:サービス登録;
|Server Application|
:サービス登録;
|Client Application|
:サービス登録結果表示;
| テスト担当者　|
:画面登録;
|Client Application|
:画面登録画面表示;
| テスト担当者　|
:画面データ入力;
|Server Application|
:画面登録;
|Client Application|
:画面登録結果表示;
| テスト担当者　|
:画面オブジェクト登録;
|Client Application|
:オブジェクト登録画面表示;
| テスト担当者　|v
:オブジェクト入力;
|Server Application|
:オブジェクト登録;
|Client Application|
:オブジェクト登録結果表示;
| テスト担当者　|
:シナリオ作成;
|Client Application|
:シナリオ登録画面表示;
| テスト担当者　|
:シナリオ入力;
|Server Application|
:シナリオ登録;
|Client Application|
:シナリオ登録結果表示;
| テスト担当者　|
:テスト登録;
|Client Application|
:テスト登録画面表示;
| テスト担当者　|
:テスト入力;
|Server Application|
:テスト登録;


end
@enduml
```

## Project 構成

### Project

```plantuml
@startuml

package Testers [
  Client_Project
  
  ---
  Server_Project
]
package Client_Project [
  
  Desktop Application

]
package Server_Project [

  Rails API Application

]
Testers *-- Client_Project
Testers *-- Server_Project
@enduml
```

### 通信

```plantuml
@startuml
left to right direction
rectangle "Client Application"{
	rectangle "画面"as screen
	rectangle "Angular/Http"as http

}
rectangle "Server Application" {
  rectangle "controllers"as controller
  database 開発環境
}
screen *-- http
http <|-.-|> controller: Internet Ajax通信
controller *-- 開発環境
@enduml
```