import { Component, OnInit } from '@angular/core';

import { Http, URLSearchParams, Headers } from '@angular/http';

@Component({
  selector: 'app-objects',
  templateUrl: './objects.component.html',
  styleUrls: ['./objects.component.css']
})
export class ObjectsComponent implements OnInit {
  objects: JSON;
  constructor(private http: Http) { }

  ngOnInit() {
    this.http.get('http://localhost:3000/screen_objects', {
      headers: new Headers({'Content-Type': 'application/json'})
    }).subscribe(
      response => {
        console.log(response.toString());
        this.objects = response.json();
        console.log(this.objects);
      },
      error => {
        this.objects = null;
      }
    );
  }

}
