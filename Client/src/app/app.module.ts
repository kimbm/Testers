import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
// material
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
// components
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ServicesComponent } from './services/services.component';
import { ScreensComponent } from './screens/component/screens.component';
import { ObjectsComponent } from './objects/component/objects.component';
import { ScenarioComponent } from './scenario/scenario.component';
import { TestsComponent } from './tests/tests.component';

// routes
import { TESTERS_ROUTES } from './app.routes';

// services
import { ScreensService } from './screens/service/screens.service';
import { ErrorComponent } from './error/component/error.component';
import { ScreenDetailComponent } from './screens/screen-detail/screen-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    ServicesComponent,
    ScreensComponent,
    ObjectsComponent,
    ScenarioComponent,
    TestsComponent,
    ErrorComponent,
    ScreenDetailComponent,
  ],
  entryComponents: [
    ScreenDetailComponent,
  ],
  imports: [
    TESTERS_ROUTES,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MatDialogModule,
    FormsModule,
    HttpModule,

  ],
  providers: [
    ScreensService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
