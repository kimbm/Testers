import { ScreenDetailComponent } from './../screen-detail/screen-detail.component';
import { Component, OnInit } from '@angular/core';
import { Screen } from '../domain/screen';
import { ScreensService } from '../service/screens.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-screens',
  templateUrl: './screens.component.html',
  styleUrls: ['./screens.component.css']
})
export class ScreensComponent implements OnInit {
  screens = [
    {id: 1, screen_id: 1, name: 'Test Screen 1', url: 'http://localhost:3000/services.json'},
    {id: 2, screen_id: 1, name: 'Test Screen 2', url: 'http://localhost:3000/services.json'},
    {id: 3, screen_id: 1, name: 'Test Screen 3', url: 'http://localhost:3000/services.json'},
    {id: 4, screen_id: 1, name: 'Test Screen 4', url: 'http://localhost:3000/services.json'},
    {id: 5, screen_id: 1, name: 'Test Screen 5', url: 'http://localhost:3000/services.json'},
  ];

  constructor(
    private screensService: ScreensService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // this.screens = this.screensService.getScreens();
  }
  // 画面詳細クリック
  screenDetail(id: any) {
    console.log(this.screens[id - 1]);
    this.dialog.open(ScreenDetailComponent, {
      data: {screen: this.screens[id - 1]}
    });
  }
}
