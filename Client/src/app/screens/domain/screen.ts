export class Screen {
  id: Number;               // 画面ID
  service_id: Number;       // サービスID
  name: String;             // 画面名
  url: String;              // 接続URL
  url_development: String;  // 開発環境URL
  url_staging: String;      // ステージング環境URL
  url_producation: String;  // 商用URL
  note: String;             // ノート
  created_at: Date;         // 作成日付
  updated_at: Date;         // 更新日付
  constructor(
  id: Number,               // 画面ID
  service_id: Number,       // サービスID
  name: String,             // 画面名
  url: String,              // 接続URL
  url_development: String,  // 開発環境URL
  url_staging: String,      // ステージング環境URL
  url_producation: String,  // 商用URL
  note: String,             // ノート
  created_at: Date,         // 作成日付
  updated_at: Date,         // 更新日付
  ) {}
}
