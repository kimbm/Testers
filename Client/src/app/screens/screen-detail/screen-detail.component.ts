import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Screen } from '../domain/screen';

@Component({
  selector: 'app-screen-detail',
  templateUrl: './screen-detail.component.html',
  styleUrls: ['./screen-detail.component.css']
})
export class ScreenDetailComponent implements OnInit {
  // 画面データ変数
  screen: Screen;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
    this.screen = new Screen(
      data.screen.id,
      data.screen.screen_id,
      data.screen.name,
      data.screen.url,
      data.screen.url_developmenet,
      data.screen.url_staging,
      data.screen.url_developmenet,
      data.screen.note,
      data.screen.created_at,
      data.screen.updated_at
    );
  }

  ngOnInit() {
    console.log('Loding : ScreenDetailComponent');
  }

}
