import { TestBed, inject } from '@angular/core/testing';

import { ScreensService } from './screens.service';

describe('ScreensService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScreensService]
    });
  });

  it('should be created', inject([ScreensService], (service: ScreensService) => {
    expect(service).toBeTruthy();
  }));
});
