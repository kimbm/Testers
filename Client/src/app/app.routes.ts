/**
 * @author Kimbm <kimbm@outlook.jp>
 * @description Route定数クラス
 */

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreensComponent } from './screens/component/screens.component';
import { ObjectsComponent } from './objects/component/objects.component';
import { ErrorComponent } from './error/component/error.component';

// ルーティングテーブル
const routes = [
  { path: 'screens', component: ScreensComponent },
  { path: 'objects', component: ObjectsComponent },
  { path: '**', component: ErrorComponent }
];

// ルートモジュール生成
export const TESTERS_ROUTES: ModuleWithProviders = RouterModule.forRoot(routes);
